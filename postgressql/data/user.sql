INSERT INTO users (name, login, password_hash, token, email, home_point, drive_hours_per_day, drive_stop_period, eat_stop_period, status ) VALUES  
(
	'Oleksii', 
	'Oleksii', 
	'adwqwqfafasf', 
	'b08f86af-35da-48f2-8fab-cef3904660bd', 
	'asda@asdasd.com', 
	'{ "name": "Rixos The Palm Dubai", "position": "[25.1212, 55.1535]"}',
	4,
	1,
	0.5,
	true
),

(
	'Vasia', 
	'Vasia', 
	'adwqwqfaasdahsdfhfasf', 
	'b096234f86af-35da-48f2-8fab-cef3904660bd', 
	'asdasfaa@asdaasfsd.com', 
	'{ "name": "Rivne", "position": "[22.1212, 54.1535]"}',
	1,
	1,
	0.3,
	false
),

(
	'Olesia', 
	'Olesia', 
	'ad112514aas412sdfhfasf', 
	'b096234af-35da-48f2-83de-cefhkyt04660bd', 
	'asdasfaa@asdafsd.com', 
	'{ "name": "Rivne", "position": "[22.1212, 54.1535]"}',
	1,
	1,
	0.3,
	false
),

(
	'Max', 
	'Max', 
	'ad1adfh25412sdfhfasf', 
	'12ss56234af-35da-48f2-83de-cefhkyt04660bd', 
	'aqqt@a254fsd.com', 
	'{ "name": "Kyiv", "position": "[21.1212, 54.1545]"}',
	2,
	1,
	0.1,
	false
),

(
	'Oleg', 
	'Oleg', 
	'adfafghj1adfh25412sdfhfasf', 
	'1q4556234af-35da-48f2-83de-cefhkyt04660bd', 
	'12dr@a254fsd.com', 
	'{ "name": "Chernigiv", "position": "[20.1212, 54.1545]"}',
	0.5,
	1,
	0.8,
	true
),

(
	'Masha', 
	'Masha', 
	'adffafw4h25412sdfhfasf', 
	'1q4556234af-35da-48f2-83de-ceafw2yt04660bd', 
	'12dr@a254fsd.com', 
	'{ "name": "Luck", "position": "[16.1212, 54.1545]"}',
	0.5,
	2,
	0.4,
	false
),

(
	'Alina', 
	'Alina', 
	'adfhahsd423412sdfhfasf', 
	'1q4556234af-35da-48f2-83de-ceafw2y15670bd', 
	'12dr@a224.com', 
	'{ "name": "Lviv", "position": "[11.1212, 54.1545]"}',
	0.1,
	1,
	0.3,
	true
),

(
	'Andrii', 
	'Andrii', 
	'adfha26727sdfhfasf', 
	'1q455rwe5af-35da-48f2-83de-ceafw2y15670bd', 
	'12ddasgr@1554.com', 
	'{ "name": "Lviv", "position": "[11.1212, 54.1234]"}',
	0.3,
	1.2,
	0.4,
	false
),

(
	'Vova', 
	'Vova', 
	'qwerty1', 
	'1q455rqarqaf-35da-48f2-83de-ceafw2y15670bd', 
	'12ddasg24@1554.com', 
	'{ "name": "Kyiv", "position": "[15.1212, 54.1234]"}',
	0.1,
	1.6,
	0.8,
	false
),

(
	'Pavlo', 
	'Pavlo', 
	'qw5115151', 
	'1q455r4456rqaf-35da-48f2-83de-ceafw2y15670bd', 
	'1212354sg24@1554.com', 
	'{ "name": "Doneck", "position": "[15.1212, 54.1234]"}',
	0.1,
	1.1,
	0.1,
	false
);