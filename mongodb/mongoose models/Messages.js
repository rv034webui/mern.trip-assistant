{
    "_id": {
        "type": "Schema.Types.ObjectId"
    },
    "chat": {
        "type": "Schema.Types.ObjectId"
    },
    "create_at": {
        "type": "Date"
    },
    "update_at": {
        "type": "Date"
    },
    "user_id": {
        "type": "Number"
    }
}